using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleController : MonoBehaviour
{
    public float timeScale = 1.0f;

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = timeScale;
    }
}
