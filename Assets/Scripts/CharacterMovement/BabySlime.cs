﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class BabySlime : CharacterLogic
{

    public float maxHp = 100.0f;
    public float currHp = 0.0f;

    [System.Serializable]
    public class LavaParams
    {
        public float hpPerTouch = 2.0f;
        public float timeBetweenLifeTick = 0.2f;
        public float currLifeTick = 0.0f;
        public float hpInLavaPerTick = 10.0f;
    }

    [System.Serializable]
    public class BounceShakeParams
    {
        public float time = 0.1f;
        public float intensity = 4.0f;
    }

    CharacterController2DKinematic cc;
    public LavaParams lavaParams;
    public UIController uiController;
    public FrostEffect frost;
    public CinemachineBrain cinemachineBrain;
    public BounceShakeParams bounceShakeParams;
    public float timeInDeath = 0.1f;


    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController2DKinematic>();
        uiController = GameObject.FindGameObjectWithTag("UiCanvas").GetComponent<UIController>();
        frost = Camera.main.GetComponent<FrostEffect>();
        cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        currHp = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        if(cc.playerState.inLava)
            currHp += lavaParams.hpInLavaPerTick;

        if(cc.playerState.grounded)
        {
            if (lavaParams.currLifeTick > lavaParams.timeBetweenLifeTick && !cc.playerState.inLava && !cc.playerState.bounce)
            {
                currHp -= lavaParams.hpPerTouch;
                lavaParams.currLifeTick = 0.0f;
            }
        }

        lavaParams.currLifeTick += Time.deltaTime;

        currHp = Mathf.Clamp(currHp, 0.0f, maxHp);
        uiController.bar = currHp / maxHp;
        frost.FrostAmount = 1 - currHp / maxHp;

        //if no hp left kill slime
        if (currHp <= 0.01f) {
            frost.FrostAmount = 1.0f;
            Die();
        }
    }
    public override void touchedExplosiveTile() {
        Die();
        cc.currentRoom.GetComponent<Room>().respawn();
    }

    public override void touchedTile() {
        if (cc.playerState.bounce)
        {
            currHp -= lavaParams.hpPerTouch;
            //cinemachineBrain.ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CameraShake>().ShakeCamera(bounceShakeParams.intensity, bounceShakeParams.time);
        }
    }
}
