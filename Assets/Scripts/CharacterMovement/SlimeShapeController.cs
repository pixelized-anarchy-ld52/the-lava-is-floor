using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class SlimeShapeController : MonoBehaviour
{
    [SerializeField] public SpriteShapeController spriteShape;
    [SerializeField] public List<Transform> points;
    [SerializeField] public Transform centerPoint;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void UpdateVertices()
    {
        setSplinePosition(0,Vector2.up);
        setSplinePosition(1, Vector2.up);
        setSplinePosition(2, Vector2.right);
        setSplinePosition(3, Vector2.down);
        setSplinePosition(4, Vector2.down);
        setSplinePosition(5, Vector2.left);

        /*
        for(int i = 0; i < points.Count; i++)
        {
            Vector2 vertex = points[i].localPosition;

            Vector2 towardsCenter = (vertex).normalized;

            float colliderRadius = points[i].gameObject.GetComponent<CircleCollider2D>().radius;

            Vector2 lt = spriteShape.spline.GetLeftTangent(i);

            Vector2 newRt = Vector2.Perpendicular((points[i].position - transform.position).normalized) * lt.magnitude;
            Vector2 newLt = Vector2.zero - newRt;

            spriteShape.spline.SetLeftTangent(i, -newLt);
            spriteShape.spline.SetRightTangent(i, -newRt);
            
        }
        */
    }
       
    //Set spline position according to a specific direction
    private void setSplinePosition(int index, Vector2 direction) {
        Vector2 vertex = points[index].localPosition;

        float colliderRadius = points[index].gameObject.GetComponent<CircleCollider2D>().radius;

        spriteShape.spline.SetPosition(index, vertex + direction * colliderRadius);

        Vector2 lt = spriteShape.spline.GetLeftTangent(index);

        Vector2 newRt = Vector2.Perpendicular((points[index].position - centerPoint.position).normalized) * lt.magnitude;
        Vector2 newLt = Vector2.zero - newRt;

        //spriteShape.spline.SetLeftTangent(index, -newLt);
        //spriteShape.spline.SetRightTangent(index, -newRt);
    }
}
