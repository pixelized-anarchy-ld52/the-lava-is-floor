﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Utils;

public partial class CharacterController2DKinematic : CharacterController
{
    [System.Serializable]
    public class Physics { 
        public float xDragGround = 0.5f;
        public float xDragAir = 0.5f;
        public float maxVelX = 22.0f;
        public float maxVelY = 22.0f;
        public float gravity = 100.0f;
        public float minGroundNormalY = .65f; 
        public float shellRadius = 0.01f;
        public bool useGravity = true;
        public float bounciness = 0.6f;
        [System.NonSerialized] public Rigidbody2D rb;
        [System.NonSerialized] public ContactFilter2D contactFilter;
        [System.NonSerialized] public float minMoveDistance = 0.001f;
    }

    [System.Serializable]
    public class PlayerState
    {
        public bool grounded = false;
        public bool preparingDash = false;
        public bool dashing = false;
        public bool bounce = false;
        public bool isBouncing = false;
        public bool facingRight = true;
        public bool inLava = false;
        public bool destroyablePlatformImpulse = false;
        public bool forceDash = false;
    }

    [System.Serializable]
    public class WalkingParams
    {
        public float runAccelGround = 1000.0f;
        public float runAccelAir = 1000.0f;
        public float runReduce = 400.0f;
    }

    [System.Serializable]
    public class DashParams
    {
        public AnimationCurve dashSpeedOverTime;
        public float dashSpeedMultiplier = 3.0f;
        public List<float> dashTimes;
        public float dashTime = 0.15f;
        public float dashTimeAfterWall = 0.15f;
        public float timeAfterDash = 0.15f;
        public float timeDashDeactivated = 0.15f;
        public Color dashColor = Color.white;
        public Color dashColorGlow = Color.white;
        public float extraDashTimeDestroyablePlatforms = 1.0f;
        public float extraVelocityDestroyablePlatformsMultiplier = 2.0f;
        public Color dashColorDestroyablePlatform = Color.white;
        public Color dashColorDestroyablePlatformGlow = Color.white;
        public bool canDash = true;
        public float timePrepareDash = 0.15f;
        [System.NonSerialized] public Vector2 dashDir;
        public Vector2 forceDashDir;
    }

    [System.Serializable]
    public class Sounds
    {
        [SerializeField] public AudioSource dashSound;
        [SerializeField] public AudioSource walkingSound;
        [SerializeField] public AudioSource bounceSound;
        [System.NonSerialized] public bool walkingSoundPlaying = false;
    }

    public Transform spriteTransform;
    public GameObject BounceParticleSystem;
    public Physics physics;
    public PlayerState playerState;
    public WalkingParams walkingParams;
    public DashParams dashParams;
    public Sounds sounds;
    private Animator anim;
    public TrailRenderer trail1;
    private Material trail1Material;
    public TrailRenderer trail2;
    private Material trail2Material;
    public float timeInBounce = 0.15f;
    private IEnumerator dashRotine;
    private IEnumerator activateDashRotine;
    private Material mat;
    private float timeInPrepareDash = 0.0f;
    private Vector2 prevMoveDir;
    private CharacterLogic characterLogic;
    [SerializeField] public GameObject currentRoom;
    [SerializeField] public float cameraShakeTime = 0.2f;
    [SerializeField] public float cameraShakeIntensityMultiplier = 1.0f/4.0f;
    private BoxCollider2D collider;
    public int numCollisionTestIterations = 8;

    private Coroutine destroyablePlatformForceCoroutine;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        physics.rb = GetComponent<Rigidbody2D>();
        physics.rb.velocity = new Vector2(0.0f, 0.0f);

        physics.contactFilter.useTriggers = false;
        physics.contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        physics.contactFilter.useLayerMask = true;

        anim = GetComponentInChildren<Animator>();
        mat = GetComponentInChildren<SpriteRenderer>().material;
        characterLogic = GetComponent<CharacterLogic>();
        mat.EnableKeyword("_HsvSaturation");
        mat.SetFloat("_HsvSaturation", 1.0f);
        trail1Material = trail1.GetComponent<TrailRenderer>().material;
        trail1Material.SetColor("_Color", dashParams.dashColor);
        trail1Material.SetColor("_GlowColor", dashParams.dashColorGlow);
        trail2Material = trail2.GetComponent<TrailRenderer>().material;
        trail2Material.SetColor("_Color", dashParams.dashColorDestroyablePlatform);
        trail2Material.SetColor("_GlowColor", dashParams.dashColorDestroyablePlatformGlow);
        collider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        timeInPrepareDash += Time.deltaTime;

        if (!playerState.grounded)
        {

            if (physics.rb.velocity.magnitude < 0.1f)
            {
                sounds.walkingSound.Stop();
                sounds.walkingSoundPlaying = false;
            }
            else if (!sounds.walkingSoundPlaying)
            {
                sounds.walkingSound.Play();
                sounds.walkingSoundPlaying = true;
            }
        }
        else
        {
            //anim.SetBool("OnAirUp", false);
            //anim.SetBool("OnAirDown", false);
        }

        if (dashParams.canDash)
            mat.SetFloat("_HsvSaturation", 1.0f);
        else
            mat.SetFloat("_HsvSaturation", 0.0f);
    }

    public override void Move(Vector2 input, bool jump, bool dash, Vector2 moveDir)
    {
        anim.SetBool("HitWall", false);
        anim.SetBool("Dash", false);

        if (playerState.grounded)
            anim.SetBool("IsWalking", Mathf.Abs(input.x) > 0.0f);

        if(!playerState.dashing && !playerState.preparingDash) { 
            if (input.x > 0 && !playerState.facingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (input.x < 0 && playerState.facingRight)
            {
                // ... flip the player.
                Flip();
            }

            float runAccel = playerState.grounded ? walkingParams.runAccelGround : walkingParams.runAccelAir;
            physics.rb.velocity += new Vector2(input.x * runAccel * Time.deltaTime, 0.0f);
        }
        
        if ((dash && dashParams.canDash) || playerState.forceDash)
        {
            timeInPrepareDash = 0.0f;
            playerState.preparingDash = true;
            physics.useGravity = false;
            playerState.forceDash = false;
            physics.rb.velocity = new Vector2(0.0f, 0.0f);
            prevMoveDir = new Vector2(0.0f, 0.0f);
            anim.SetBool("Dash", true);
            sounds.dashSound.Play();
        }

        if (playerState.preparingDash && timeInPrepareDash > dashParams.timePrepareDash)
        {
            playerState.preparingDash = false;

            if (playerState.destroyablePlatformImpulse)
                dashParams.dashDir = dashParams.forceDashDir;
            else if (moveDir.magnitude > 0.01f)
                dashParams.dashDir = moveDir.normalized;
            else if (prevMoveDir.magnitude > 0.01f)
            {
                dashParams.dashDir = prevMoveDir.normalized;
            }
            else
                dashParams.dashDir = playerState.facingRight ? new Vector2(1.0f, 0.0f) : new Vector2(-1.0f, 0.0f);

            physics.rb.velocity = dashParams.dashDir * dashParams.dashSpeedMultiplier * dashParams.dashSpeedOverTime.Evaluate(0.0f);

            if (dashRotine != null)
                StopCoroutine(dashRotine);
            dashRotine = dashCoroutine();
            StartCoroutine(dashRotine);
        }

        playerState.bounce = false;
        anim.SetBool("OnAirDown", false);
        anim.SetBool("OnAirUp", false);
        playerState.grounded = false;

        //gravity
        if (physics.useGravity)
            physics.rb.velocity -= new Vector2(0.0f, physics.gravity * Time.deltaTime);

        //clamp to max velocity
        if (!playerState.dashing && !playerState.preparingDash)
        {
            physics.rb.velocity = new Vector2(Mathf.Clamp(physics.rb.velocity.x, -physics.maxVelX, physics.maxVelX), 
                                              Mathf.Max(physics.rb.velocity.y, -physics.maxVelY));
        }

        //ideal move based on current velocity
        Vector2 move = physics.rb.velocity * Time.deltaTime;

        if(playerState.dashing)
        {
            List<RaycastHit2D> hits = testMove(move, true);
            checkSpecialObjectCollision(hits, move);
            bool bounce = checkBounce(hits, true);
            treatMovement(hits, new Vector2(move.x, move.y));
        }
        else {
            Vector2 moveX = new Vector2(move.x, 0.0f);
            Vector2 moveY = new Vector2(0.0f, move.y);
            
            //process collisions against other objects in the scene in the vertical direction
            List<RaycastHit2D> hitsVer = testMove(moveY, false);
            //process collisions against other objects in the scene in the horizontal direction
            List<RaycastHit2D> hitsHor = testMove(moveX, true);
            checkSpecialObjectCollision(hitsVer, moveY);
            checkSpecialObjectCollision(hitsHor, moveX);

            bool bounce = checkBounce(hitsVer, false);
            bool bounceHor = checkBounce(hitsHor, true);

            treatMovement(hitsVer, new Vector2(0.0f, move.y));
            treatMovement(hitsHor, new Vector2(move.x, 0.0f));
        }

        float xDrag = playerState.grounded ? physics.xDragGround : physics.xDragAir;
        physics.rb.velocity = new Vector2(physics.rb.velocity.x * xDrag, physics.rb.velocity.y);

        if (!playerState.grounded && !playerState.isBouncing && !playerState.bounce) {
            anim.SetBool("OnAirUp", false);
            anim.SetBool("OnAirDown", false);

            if (physics.rb.velocity.y > 0.0f) {
                anim.SetBool("OnAirUp", true);
                spriteTransform.up = physics.rb.velocity.normalized;
            }
            else if(physics.rb.velocity.y < 0.0f ) {
                anim.SetBool("OnAirDown", true);
                spriteTransform.up = -physics.rb.velocity.normalized;
            }
            else
            {
                anim.SetBool("OnAirDown", true);
            }
        }
        else if(playerState.grounded)
            spriteTransform.up = Vector3.up;

        anim.SetBool("Grounded", playerState.grounded);
        prevMoveDir = moveDir;
    }

    IEnumerator dashCoroutine()
    {

        float dashTime = 0.0f;
        bool platformImpulse = false;
        //before dash
        if (playerState.destroyablePlatformImpulse)
        {
            dashTime = dashParams.dashTimes[1] + dashParams.extraDashTimeDestroyablePlatforms;
            trail1.emitting = false;
            trail2.emitting = true;
            platformImpulse = true;
            playerState.destroyablePlatformImpulse = false;
        }
        else
        {
            dashTime = dashParams.dashTimes[0];
            trail1.emitting = true;
            trail2.emitting = false;
            dashParams.canDash = false;
        }

        float timeInDash = 0.0f;
        int numBounces = 0;
        playerState.dashing = true;

        //during dash
        while (timeInDash < dashTime)
        {
            physics.rb.velocity = dashParams.dashDir * dashParams.dashSpeedMultiplier * dashParams.dashSpeedOverTime.Evaluate(timeInDash / dashTime);

            if(platformImpulse) {
                physics.rb.velocity *= dashParams.extraVelocityDestroyablePlatformsMultiplier;
            }

            if (playerState.bounce || playerState.destroyablePlatformImpulse)
            {
                trail1.emitting = false;
                trail2.emitting = false;

                Vector2 prevBounce = physics.rb.velocity;
                physics.rb.velocity = new Vector2(0.0f, 0.0f);
                playerState.isBouncing = true;
                yield return new WaitForSeconds(timeInBounce);
                trail1.emitting = true;
                playerState.isBouncing = false;
                physics.rb.velocity = prevBounce;
                timeInDash = 0.0f;

                //if we bounced against a destroyable platform, add extra velocity and apply the second bounce durantion
                if (playerState.destroyablePlatformImpulse)
                {
                    //artificially set the number of bounces to 0 to account for the extra powar
                    numBounces = 0;
                    dashTime = dashParams.dashTimes[1] + dashParams.extraDashTimeDestroyablePlatforms;
                    trail2.emitting = true;
                    trail1.emitting = false;
                    platformImpulse = true;
                    playerState.destroyablePlatformImpulse = false;

                    //Special bounce, don't count
                    //numBounces++;
                }
                else
                {
                    numBounces++;
                    dashTime = dashParams.dashTimes[numBounces];
                    //bounced against another object, deactivate dash impulse
                    platformImpulse = false;
                }
            }
            yield return new WaitForFixedUpdate();
            timeInDash += Time.fixedDeltaTime;
        }

        spriteTransform.up = Vector3.up;
        physics.rb.velocity = new Vector2(0.0f, 0.0f);
        trail1.emitting = false;
        trail2.emitting = false;
        platformImpulse = false;

        yield return new WaitForSeconds(dashParams.timeAfterDash);

        //after dash
        playerState.dashing = false;
        physics.useGravity = true;
        playerState.destroyablePlatformImpulse = false;
    }

    public List<RaycastHit2D> testMoveDash(Vector2 move, bool horizontal)
    {
        List<RaycastHit2D> raycastHits = new List<RaycastHit2D>();
        Vector3 CenterOffset = new Vector3(collider.size.x, 0.0f, 0.0f);
        Vector3 TopOffset = new Vector3(collider.size.x, collider.size.y, 0.0f);
        Vector3 BottomOffset = new Vector3(collider.size.x, -collider.size.y, 0.0f);
        float faceDir = playerState.facingRight ? -1.0f : 1.0f;

        RaycastHit2D collision = rayCast(transform.position, move.normalized, move.magnitude, physics.contactFilter.layerMask);
        if (collision.collider)
        {
            raycastHits.Add(collision);
        }

        /*
        collision = rayCast(transform.position + TopOffset * faceDir, move.normalized, move.magnitude, physics.contactFilter.layerMask);
        if (collision.collider)
        {
            raycastHits.Add(collision);
        }

        collision = rayCast(transform.position + BottomOffset * faceDir, move.normalized, move.magnitude, physics.contactFilter.layerMask);
        if (collision.collider)
        {
            raycastHits.Add(collision);
        }
        */

        return raycastHits;
            /*
            List<RaycastHit2D> raycastHits = new List<RaycastHit2D>();
            Dictionary<Collider2D, Collider2D> raycastHitsDict = new Dictionary<Collider2D, Collider2D>();
            Vector2 dir = move.normalized;
            float distancePerIteration = (move.magnitude) / (float) numCollisionTestIterations;
            float currentDistance = 0.0f;

            for (int i = 0; i < numCollisionTestIterations; i++)
            {
                Vector2 pos = new Vector2(transform.position.x, transform.position.y) + dir * currentDistance;
                RaycastHit2D collision = PlaceMeet(pos.x, pos.y, physics.contactFilter.layerMask);
                if (collision.collider && !raycastHitsDict.ContainsKey(collision.collider))
                {
                    raycastHits.Add(collision);
                    raycastHitsDict.Add(collision.collider, collision.collider);
                }
                currentDistance += distancePerIteration;
            }

            return raycastHits;
            */

        /*
        float distanceMoved = move.magnitude;
        List<RaycastHit2D> raycastHits = new List<RaycastHit2D>();
        Vector2 pos = new Vector2(transform.position.x, transform.position.y);
        Vector2 direction = move;
        float minSafeDistance = 2.0f * Mathf.Min(collider.size.x, collider.size.y);
        float distance = 0.0f;
        RaycastHit2D hitInfo;

        PlaceMeet(transform.position.x + move.x, transform.position.y, physics.contactFilter.layerMask)

        if (distanceMoved > physics.minMoveDistance)
        {
            for(int i = 0; i < (int) Mathf.Max(distanceMoved / minSafeDistance, 1);  i++) { 
                hitInfo = Physics2D.BoxCast(pos, collider.size, 0.0f, direction.normalized * distance);
                if (hitInfo.collider != null && !hitInfo.transform.CompareTag("Player"))
                {
                    Debug.Log(hitInfo.point);
                    raycastHits.Add(hitInfo);
                    distance += hitInfo.distance;
                }
            }

        }

        return raycastHits;
        */

        /* Old version
        float distanceMoved = move.magnitude;
        List<RaycastHit2D> raycastHits = new List<RaycastHit2D>();

        if (distanceMoved > physics.minMoveDistance)
        {
            RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
            int count = physics.rb.Cast(move, physics.contactFilter, hitBuffer, distanceMoved + physics.shellRadius);
            for (int i = 0; i < count; i++)
                raycastHits.Add(hitBuffer[i]);

        }

        return raycastHits;
        */

        }

    public List<RaycastHit2D> testMove(Vector2 move, bool horizontal)
    {
        float distanceMoved = move.magnitude;
        List<RaycastHit2D> raycastHits = new List<RaycastHit2D>();

        if (distanceMoved > physics.minMoveDistance)
        {
            RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
            int count = physics.rb.Cast(move, physics.contactFilter, hitBuffer, distanceMoved + physics.shellRadius);
            for (int i = 0; i < count; i++)
                raycastHits.Add(hitBuffer[i]);

        }

        return raycastHits;
    }

    public bool checkBounce(List<RaycastHit2D> hits, bool horizontal)
    {
        bool res = false;

        for (int i = 0; i < hits.Count; i++)
        {
            Vector2 currentNormal = hits[i].normal;
            if (currentNormal.y > physics.minGroundNormalY)
            {
                dashParams.canDash = true;
                playerState.grounded = true;
            }

            float projection = Vector2.Dot(physics.rb.velocity, currentNormal);

            //Bounce
            if ((projection < -6 || playerState.dashing) && !playerState.bounce)
            {
                Instantiate(BounceParticleSystem, new Vector3(hits[i].point.x, hits[i].point.y, 0.0f), Quaternion.identity);
                sounds.bounceSound.Play();

                playerState.bounce = true;
                currentRoom.GetComponent<Room>().cameraFocus.GetComponent<CameraShake>().ShakeCamera(cameraShakeIntensityMultiplier * physics.rb.velocity.magnitude, cameraShakeTime);
                physics.rb.velocity = Vector2.Reflect(physics.bounciness * physics.rb.velocity, currentNormal);
                dashParams.dashDir = Vector2.Reflect(dashParams.dashDir, currentNormal);
                spriteTransform.up = currentNormal.normalized;

                characterLogic.touchedTile();

                if (activateDashRotine != null)
                    StopCoroutine(activateDashRotine);

                activateDashRotine = WaitAndDo(dashParams.timeDashDeactivated,
                                                () => {
                                                    dashParams.canDash = true;
                                                    activateDashRotine = null;
                                                });
                StartCoroutine(activateDashRotine);

                anim.SetBool("HitWall", true);

                res = true;
            }
        }

        return res;
    }

    public void treatMovement(List<RaycastHit2D> hits, Vector2 move)
    {
        float distanceMoved = move.magnitude;

        for (int i = 0; i < hits.Count; i++)
        {
            Vector2 currentNormal = hits[i].normal;
            float projection = Vector2.Dot(physics.rb.velocity, currentNormal);

            //treat collision
            if (projection < 0)
            {
                physics.rb.velocity -= projection * currentNormal;
            }

            float modifiedDistance = hits[i].distance - physics.shellRadius;
            distanceMoved = modifiedDistance < distanceMoved ? modifiedDistance : distanceMoved;

            /*
            if(!playerState.grounded)
                Debug.Log(physics.rb.position + move.normalized * distanceMoved);
            */
        }

        physics.rb.position = physics.rb.position + move.normalized * distanceMoved;
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        playerState.facingRight = !playerState.facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    private void checkSpecialObjectCollision(List<RaycastHit2D> hits, Vector2 move)
    {
        for (int i = 0; i < hits.Count; i++)
        {
            RecieveColideEvent re = hits[i].transform.GetComponent<RecieveColideEvent>();
            if (re != null)
            {
                re.EventAt(hits[i].point + move.normalized * 0.3f, gameObject);
            }

            if(hits[i].transform.CompareTag("DestroyablePlatform") &&
                hits[i].transform.GetComponent<DestroyablePlatforms>().platformState == DestroyablePlatforms.PlatformState.Destroyed)
            {

                if(!playerState.dashing) { 
                    DestroyablePlatforms destroyablePlatform = hits[i].transform.GetComponent<DestroyablePlatforms>();

                    if (destroyablePlatformForceCoroutine != null)
                        StopCoroutine(destroyablePlatformForceCoroutine);

                    destroyablePlatformForceCoroutine = StartCoroutine(DestroyablePlatformForceCoroutine(destroyablePlatform, hits[i].normal));

                }
                else { 
                    DestroyablePlatforms destroyablePlatform = hits[i].transform.GetComponent<DestroyablePlatforms>();

                    playerState.destroyablePlatformImpulse = true;

                    if(!playerState.dashing) {
                        //trigger dash
                        playerState.forceDash = true;
                        dashParams.forceDashDir = hits[i].normal;
                    }
                }

                if (activateDashRotine != null)
                    StopCoroutine(activateDashRotine);

                activateDashRotine = WaitAndDo(dashParams.timeDashDeactivated,
                                                () => {
                                                    dashParams.canDash = true;
                                                });
                StartCoroutine(activateDashRotine);
            }
        }
    }
    private IEnumerator DestroyablePlatformForceCoroutine(DestroyablePlatforms destroyablePlatform, Vector2 normal)
    {
        float time = 0.0f;
        float explosionTime = destroyablePlatform.explosionForceTime;

        while (time < explosionTime) {
            if(normal.y > 0.0f)
                physics.rb.velocity = new Vector2(physics.rb.velocity.x, 
                                                  destroyablePlatform.explosionForceOverTime.Evaluate(time / explosionTime) * destroyablePlatform.explosionForceMultiplier * normal.y);
            else
                physics.rb.velocity = new Vector2(normal.x * destroyablePlatform.explosionForceOverTime.Evaluate(time / explosionTime) * destroyablePlatform.explosionForceMultiplier, 
                                                  physics.rb.velocity.y * 0.5f);

            yield return new WaitForFixedUpdate();
            time += Time.fixedDeltaTime;
        }
    }

    private float approach(float val, float target, float maxMove)
    {
        return val > target ? Mathf.Max(val - maxMove, target) : Mathf.Min(val + maxMove, target);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Lava"))
        {
            playerState.inLava = true;
        }

        if(collision.gameObject.CompareTag("Room"))
        {
            currentRoom = collision.gameObject;
            Room room = collision.gameObject.GetComponent<Room>();
            if(room.usesConfiner)
                room.cameraFocus.Follow = transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Lava"))
        {
            playerState.inLava = false;
        }
    }
    private RaycastHit2D PlaceMeet(float xPos, float yPos, LayerMask mask)
    {
        Vector2 position = new Vector2(xPos + collider.offset.x, yPos + collider.offset.y);
        RaycastHit2D rayHit = Physics2D.BoxCast(position, collider.size, 0.0f, new Vector2(), 0, mask);

        if (!playerState.grounded && playerState.dashing && rayHit.collider)
            Debug.Log(rayHit.point);

        if (rayHit.collider) {
            ExtDebug.DrawBox(position, collider.size, Quaternion.identity, Color.red);
        }
        else {
            ExtDebug.DrawBox(position, collider.size, Quaternion.identity, Color.green);
        }

        return rayHit;
    }

    private RaycastHit2D rayCast(Vector2 pos, Vector2 dir, float dist, LayerMask mask)
    {
        RaycastHit2D rayHit = Physics2D.Raycast(pos, dir, dist, mask);

        if(rayHit.collider)
            Debug.DrawRay(pos, dir * dist, Color.green);
        else
            Debug.DrawRay(pos, dir * dist, Color.red);

        return rayHit;
    }
}
