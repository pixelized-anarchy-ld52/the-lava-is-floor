﻿using System;
using System.Collections;
using UnityEngine;

public class PlayerMovement : Controllable {

    public CharacterController controller;

    public float runSpeed = 40f;

    Vector2 move = new Vector2();
    Vector2 moveDir = new Vector2();
    bool _jump = false;
    bool _dash = false;
    public bool useDashMemory = true;
    public float dashInputTime = 0.05f;

    // Update is called once per frame
    void Update () {
        if( controllable ){
            float hor = Input.GetAxisRaw("Horizontal");
            float ver = Input.GetAxisRaw("Vertical");
            if (Mathf.Abs(hor) > 0.30f)
            {
                hor = hor > 0.0f ? 1.0f : -1.0f;
            }
            else
            {
                hor = 0.0f;
            }

            if(Mathf.Abs(ver) > 0.30f)
            {
                ver = ver > 0.0f ? 1.0f : -1.0f;
            }
            else
            {
                ver = 0.0f;
            }

            moveDir = new Vector2(0.0f, 0.0f);
            move = new Vector2(0.0f, 0.0f);
            if ((new Vector2(hor, ver)).magnitude > 0.01f)
                moveDir = new Vector2(hor, ver).normalized;
            if ((new Vector2(hor, ver)).magnitude > 0.01f)
                move = (new Vector2(hor, ver)).normalized * runSpeed;

            if (Input.GetButtonDown("Jump"))
            {
                //_jump = true;
            }

            if(Input.GetButtonDown("Dash"))
            {
                _dash = true;
            }
        } 
    }

    void FixedUpdate ()
    {
        if(controllable) {

            // Move our character
            controller.Move(move * Time.fixedDeltaTime, _jump, _dash, moveDir);

            _jump = false;
            _dash = false;
        }
    }
    IEnumerator WaitAndDo(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action();
    }
}