﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MenuSlime : MonoBehaviour
{

    public Animator anim;
    public CharacterController2D ctrl;
    
    // CharacterController2D Move() parameters
    private Vector2 _ctrl_move = Vector2.zero;
    private bool _ctrl_jump = false;
    private bool _ctrl_dash = false;
    private Vector2 _ctrl_moveDir = Vector2.zero;

    void Start()
    {
        StartCoroutine(JumpOnceInAWhile());
    }

    void Update()
    {
        ctrl.Move(_ctrl_move, _ctrl_jump, _ctrl_dash, _ctrl_moveDir);
    }

    private void OnGUI()
    {
        GUI.Box(new Rect(1, 0, 300, 110), "Fiddling with CharacterController2D");
        GUI.Label(new Rect(10, 30, 280, 20), $"move: \t {_ctrl_move}");
        GUI.Label(new Rect(10, 45, 280, 20), $"jump: \t {_ctrl_jump}");
        GUI.Label(new Rect(10, 60, 280, 20), $"dash: \t {_ctrl_dash}");
        GUI.Label(new Rect(10, 75, 280, 20), $"moveDir: \t {_ctrl_moveDir}");
        GUI.Label(new Rect(10, 90, 280, 20), $"JumpForce: \t {ctrl.m_JumpForce}");
    }

    private IEnumerator JumpOnceInAWhile()
    {
        while (true)
        {
            float power = Random.Range(3.0f, 10.0f);
            yield return new WaitForSeconds(power);

            _ctrl_jump = true;
            ctrl.m_JumpForce = power * 60.0f;
            
            // Wait 5 frames
            Debug.Log("Waiting 5 frames.....");
            yield return StartCoroutine((new YieldInstruction[5]).GetEnumerator());
            Debug.Log("Done waiting");
            
            _ctrl_jump = false;
        }
    }
}
