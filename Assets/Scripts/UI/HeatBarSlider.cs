﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HeatBarSlider : MonoBehaviour
    {
        [SerializeField] private Gradient _colorGradient;
        [SerializeField] private Image _barImage;
        
        private RectTransform _rect;
        private float _fullWidth;

        void Start()
        {
            _rect = GetComponent<RectTransform>();
            _fullWidth = _rect.sizeDelta.x;
        }

        /**
         * progress = 1: Barra cheia
         * progress = 0: Barra vazia
         */
        public void SetSlider(float progress)
        {
            float width = Mathf.Lerp(0.07f, 1, progress);
            _rect.anchorMax = new Vector2(width, _rect.anchorMax.y);
            _barImage.color = _colorGradient.Evaluate(progress);
        }
    }
}