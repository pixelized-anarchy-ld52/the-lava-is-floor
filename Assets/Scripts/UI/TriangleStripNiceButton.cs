﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public partial class TriangleStrip
    {
#if UNITY_EDITOR
        [CustomEditor(typeof(TriangleStrip))]
        public class PutButtonInInspector : Editor
        {
            private static readonly GUIContent BtnValoresSugeridos = new GUIContent(
                text: "^^^^^ Preencher automaticamente ^^^^^",
                tooltip: "Este botão ajusta as referências e copia a largura da imagem para Triangle Width"
            );
            
            // ReSharper disable Unity.PerformanceAnalysis
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                if (GUILayout.Button(BtnValoresSugeridos) && target is TriangleStrip triangleStrip)
                {
                    triangleStrip.InitializeFields();
                }
            }

        }

        // Start() não é chamado quando eu quero em Edit Mode :(
        // plz carreguem no meu botão
        private void InitializeFields()
        {
            rect = GetComponent<RectTransform>();
            parentRect = transform.parent.GetComponent<RectTransform>();
            
            var texture = GetComponent<Image>();
            triangleWidth = ((float) texture.mainTexture.width) / texture.pixelsPerUnitMultiplier;
        }
#endif
    }
}