﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableWall : MonoBehaviour
{
    public GameObject From;
    public GameObject To;
    
    public float Speed = 3.0f;
    public float Time = 0.5f;

    public float threshold = 0.05f;

    public enum MoveState { Stoped, MovingToTarget, MovingToInitial } 
    public MoveState InitialState = MoveState.Stoped; 
    public MoveState CurrentState { get; private set; }
    private Vector2 vel = Vector2.zero;

    void Start() {
        CurrentState = InitialState;    
    }
    // Update is called once per frame
    void Update()
    {
        switch(CurrentState){
            case MoveState.MovingToTarget:
                Debug.Log("Moving! "+Mathf.Epsilon);
                transform.position = Vector2.SmoothDamp(transform.position, To.transform.position, ref vel, Time, Speed);
                if(Vector2.Distance(transform.position, To.transform.position) <= threshold){
                    CurrentState = MoveState.MovingToInitial;
                }
                break;
            case MoveState.MovingToInitial:
                Debug.Log("Moving Back!");
                transform.position = Vector2.SmoothDamp(transform.position, From.transform.position, ref vel, Time, Speed);
                if(Vector2.Distance(transform.position, From.transform.position) <= threshold){
                    CurrentState = MoveState.Stoped;
                }
                break;
            case MoveState.Stoped:
            default:
                break;
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
            Debug.Log("Other entered! "+other.tag);
            if( !other.CompareTag("Player") ) return;

            if( CurrentState != MoveState.Stoped) return;

            CurrentState = MoveState.MovingToTarget;
    }
}
