﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

/**
 * Prioridades:
 *    0 = O jogador NÃO está na minha sala
 *    1 = Câmera de "fallback" (configurado manualmente no Editor)
 *    2 = Câmera da 1ª sala    (configurado manualmente no Editor)
 *   10 = O jogador está na minha sala
 */
public class RoomCamTrigger : MonoBehaviour
{
    public CinemachineVirtualCamera vcam;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Controllable playableCharacter) && playableCharacter.controllable)
        {
            UseMe();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Controllable playableCharacter) && playableCharacter.controllable)
        {
            // O jogador saíu porque mudou de sala ou porque morreu?
            if (other.TryGetComponent(out CharacterLogic characterLogic) && characterLogic.isAlive)
            {
                // Está vivo. Salta já para a câmera da outra sala.
                LeaveMe();
            }
            else
            {
                // Deve ter morrido. Apreciemos a tile "explosiva" a piscar.
                StartCoroutine(Utils.WaitAndDo(1.0f, LeaveMe));
            }
        }
    }
    
    public void UseMe()
    {
        vcam.Priority = 10;
    }
    
    public void LeaveMe()
    {
        vcam.Priority = 0;
    }

}
