using System;
using UnityEngine;

public class Controllable : MonoBehaviour
{
    public bool controllable { get; private set; }
    private ToggableFocus toggable;

    public void Start() {
        toggable = Camera.main.GetComponent<ToggableFocus>();
        toggable.GameObjects.Add(this);
    }
    internal void setControllable(bool v)
    {
        controllable = v;
    }

    private void OnDestroy() {
        if( toggable != null ){
            toggable.GameObjects.Remove(this);
        }
    }
}