using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/**
 * Conta o tempo e as mortes e regista tempos por checkpoint
 */
[RequireComponent(typeof(TextMeshProUGUI))]
public class SpeedrunClock : MonoBehaviour
{
    [Serializable]
    public class CheckpointTracker
    {
        // Associa cada sala ao tempo que o jogador demorou a atravessá-la pela 1ª vez
        // As entradas de um OrderedDictionary são iteradas pela mesma ordem em que foram inseridas
        private OrderedDictionary roomTimes = new OrderedDictionary();

        private Room currentRoom = null;
        private double roomStart;
        private int respawnCount;
        
        /**
         * Chamar este método quando o jogador entrar numa sala nova
         */
        public void RoomEntered(Room enteredRoom)
        {
            // Se entrámos numa sala nova, grava o tempo da sala onde estávamos e reinicia a cronometragem
            if (currentRoom != null &&                // Ignora: 1ª sala
                currentRoom != enteredRoom &&         // Ignora: Morte ou vinda de um corredor
                !roomTimes.Contains(currentRoom) &&   // Ignora: Vinda de uma sala antiga
                !roomTimes.Contains(enteredRoom))     // Ignora: Regresso a uma sala antiga
            {
                roomTimes.Add(currentRoom, (Time.timeAsDouble - roomStart, respawnCount));
                roomStart = Time.timeAsDouble;
                respawnCount = 0;
            }
        
            currentRoom = enteredRoom;
        }
        
        public void PlayerRespawned(Room _) => respawnCount++;
        
        /**
         * Produz triplos (sala, tempo, mortes) para um foreach
         * (estou chateado porque a Microsoft não fez uma versão genérica do OrderedDictionary)
         */
        public IEnumerable<(Room, double, int)> GetTimes() =>
            from DictionaryEntry entry in roomTimes
            let room = (Room) entry.Key
            let stats = ((double time, int respawnCount)) entry.Value
            select (room, stats.time, stats.respawnCount);
        
        public void Clear() => roomTimes.Clear();
        public int Count => roomTimes.Count;


        public Room CurrentRoom => currentRoom;
        public double RoomTime => Time.timeAsDouble - roomStart;
        public int RoomRespawns => respawnCount;
    }
    
    
    public TMP_Text text;
    public CheckpointTracker checkpoints;
    public int deathCount;

    private double start_t;
    
    public RectTransform background;
    private float lastHeight = 0;

    void Start()
    {
        text = GetComponent<TMP_Text>();
        background = GetComponentInParent<Image>().rectTransform;

        checkpoints.Clear();
        
        // Por agora consideramos que as salas são troços (e o início de cada é um "checkpoint")
        // No futuro podemos ligar UnityEvents manualmente no Inspector para escolhermos
        // o que é que conta como checkpoint (e apagar este foreach)
        foreach (var room in FindObjectsOfType<Room>())
        {
            room.onEnter.AddListener(checkpoints.RoomEntered);

            // Sala onde a cena começa conta como 1º troço para a contagem
            if (room.cameraFocus.Priority > 0)
                checkpoints.RoomEntered(room);

            // Conta as mortes do jogador
            room.onRespawn.AddListener(_ => deathCount++);
            room.onRespawn.AddListener(checkpoints.PlayerRespawned);
        }
        
        start_t = Time.timeAsDouble;
    }


    void Update()
    {
        text.text = BuildMarkup();
        float targetHeight = text.bounds.size.y + (checkpoints.Count > 0 || deathCount > 0 ? 25 : 50);
        lastHeight = Mathf.Lerp(lastHeight, targetHeight, 0.1f);
        background.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, lastHeight);
    }

    private string BuildMarkup()
    {
        // Referência das tags Rich Text do TextMeshPro:
        // http://digitalnativestudios.com/textmeshpro/docs/rich-text/#color

        // Conversão de Time.time (em segundos) para "minutos:segundos.milésimos", monoespaçado
        string FormatTime(double t) =>
            $@"<mspace=0.6em>{TimeSpan.FromSeconds(t):mm\:ss\.fff}</mspace>";

        // TODO Experimentar "Stylesheets" do TextMeshPro, mas preciso de criar um asset
        const string COLOR_CUR_TIME = "<#ffffff>";
        const string COLOR_TIMES = "<#88aa33>";
        const string COLOR_DEATHS = "<#ffccaa>";

        StringBuilder markup = new StringBuilder();

        markup.AppendLine(FormatTime(Time.timeAsDouble - start_t))  // Tempo total
            .Append("<size=16>")
            .Append("<line-height=150%>")
            .Append(COLOR_CUR_TIME)
            .AppendFormat("@ {1}   <pos=50%><mark=#ccee88{2:X}><b>{0}</b></mark>    <pos=90%>{3}", 
                FormatTime(checkpoints.RoomTime),                                      // {0}   Tempo atual na sala
                checkpoints.CurrentRoom.name,                                          // {1}   Nome da sala atual
                (int)Mathf.Lerp(0xcc, 0x33, (float)checkpoints.RoomTime / 0.50f),      // {2:X} Pisca numa nova sala
                checkpoints.RoomRespawns > 0 ? $"{checkpoints.RoomRespawns:d}" : "");  // {3}   Mortes nesta sala
        
        if (checkpoints.Count > 0)
        {
            markup.AppendLine()
                .Append("<line-height=100%>");
            
            int roomNumber = 1;
            foreach ((var room, double time, int deaths) in checkpoints.GetTimes())
                markup.Append(COLOR_TIMES)
                    .Append(roomNumber == checkpoints.Count ? "<mark=#ccee8833>" : "")    // Realça a última linha
                    .Append($"#{roomNumber++:d} ({room.name}):")                          // Nº e nome da sala
                    .Append($"<pos=50%>{FormatTime(time)}")                               // Tempo da sala
                    .AppendLine(deaths > 0 ? $"<pos=90%>{COLOR_DEATHS}{deaths:d}" : "");  // Mortes na sala

            markup.Length--;  // Apaga o último \n
        }

        if (deathCount > 0)
            markup.AppendLine()
                .Append("</mark>")
                .Append("<size=16>")
                .Append("<voffset=-1em>")
                .Append(COLOR_DEATHS)
                .Append($"<pos=55%>Respawns: <pos=90%>{deathCount:d}");

        return markup.ToString();
    }
}