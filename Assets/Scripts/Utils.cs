﻿using System;
using System.Collections;
using UnityEngine;

/**
 * Como usar:
 *   1: using static Utils;
 *   2: Profit
 */
public static class Utils
{
    /// <summary>
    /// Um atalho para <c>new Vector2(x, y)</c>
    /// porque o Goucha não gosta de <c>new</c> atrás de <c>Vector2</c>
    /// </summary>
    /// <remarks>
    /// Funciona melhor com <c>using static</c>
    /// </remarks>
    public static Vector2 Vec2(float x, float y) => new Vector2(x, y);


    public static IEnumerator WaitAndDo(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action();
    }
    
    // Há mais para vir!!
}
