using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Room : MonoBehaviour
{
    [Header("Room setup")]
    [SerializeField] public GameObject spawnPoint;
    [SerializeField] public bool usesConfiner = false;
    [SerializeField] public Cinemachine.CinemachineVirtualCamera cameraFocus;
    [SerializeField] public GameObject player;
    
    [Header("Room events")]
    [SerializeField] public UnityEvent<Room> onEnter = new UnityEvent<Room>();
    [SerializeField] public UnityEvent<Room> onExit = new UnityEvent<Room>();
    [SerializeField] public UnityEvent<Room> onRespawn = new UnityEvent<Room>();

    private IEnumerator respawnRoutine;

    public void respawn()
    {
        if (respawnRoutine != null)
            StopCoroutine(respawnRoutine);
        respawnRoutine = respawnCoroutine();
        StartCoroutine(respawnRoutine);
    }

    IEnumerator respawnCoroutine()
    {
        Vector3 pos = spawnPoint.transform.position;
        yield return new WaitForSeconds(player.GetComponent<BabySlime>().timeInDeath);
        Instantiate(player, pos, Quaternion.identity);
        onRespawn.Invoke(this);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Controllable playableCharacter) && playableCharacter.controllable)
        {
            UseMe();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Controllable playableCharacter) && playableCharacter.controllable)
        {
            // O jogador saíu porque mudou de sala ou porque morreu?
            if (other.TryGetComponent(out CharacterLogic characterLogic) && characterLogic.isAlive)
            {
                // Está vivo. Salta já para a câmera da outra sala (ou para a câmera de fallback).
                LeaveMe();
            }
            else
            {
                // Deve ter morrido. Apreciemos a tile "explosiva" a piscar.
                StartCoroutine(Utils.WaitAndDo(1.0f, LeaveMe));
            }
        }
    }

    public void UseMe()
    {
        cameraFocus.Priority = 10;
        onEnter.Invoke(this);
    }

    public void LeaveMe()
    {
        cameraFocus.Priority = 0;
        onExit.Invoke(this);
    }
}
