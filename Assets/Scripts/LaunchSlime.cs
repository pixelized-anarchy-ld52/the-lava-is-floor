﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using UnityEngine;

public class LaunchSlime : MonoBehaviour
{
    public MotherSlimeMoviment MotherSlime;
    private GameObject Target;
    internal GameObject BabySlimePrefab;
    private Vector2 vel;
    public float AnimationTime = 0.3f;
    public float inc = 0.4f;

    void Start()
    {
        Target = GetComponentInChildren<SpriteRenderer>().gameObject;
    }

    internal GameObject launchBabySlime()
    {
        GameObject CurrentBabySlime = Instantiate<GameObject>(BabySlimePrefab, Target.transform.position, Quaternion.identity);
        Physics2D.IgnoreCollision(MotherSlime.GetComponent<Collider2D>(), CurrentBabySlime.GetComponent<Collider2D>(), true);
        StartCoroutine(LauchSlime(CurrentBabySlime));
        return CurrentBabySlime;
    }

    IEnumerator LauchSlime(GameObject CurrentBabySlime){
        yield return null;
        Camera.main.GetComponent<ToggableFocus>().SetActive(CurrentBabySlime);
        //CharacterController2D cc2d = CurrentBabySlime.GetComponent<CharacterController2D>();
        //Vector2 direction = Target.transform.position - CurrentBabySlime.transform.position;
		// //StartCoroutine(ScaleUp(CurrentBabySlime));
        // //cc2d.m_Dashing = true;
        // //yield return cc2d.DashWait();
        // cc2d.m_PrepareDash = true;
        // cc2d.timeInDash = 0.0f;
        // cc2d.m_Rigidbody2D.gravityScale = 0;
        // cc2d.animator.SetBool("Dash", true);
        // cc2d.dashSound.Play();
        // cc2d.prevMoveDir = direction;
        // cc2d.prevMoveLastTime = 0.0f;
        // cc2d.m_PrepareDash = false;
        // cc2d.m_Rigidbody2D.velocity = direction.normalized * cc2d.m_DashSpeed * 2;
    }

    IEnumerator ScaleUp(GameObject CurrentBabySlime){
        Vector3 target = CurrentBabySlime.transform.localScale;
        Vector3 tmp = CurrentBabySlime.transform.localScale;
        float perc = 0;
        while(perc < 1){
            tmp.y = Mathf.Lerp(0, target.y, perc);
            CurrentBabySlime.transform.localScale = tmp;
            perc+=inc;
            yield return null;
        }
        CurrentBabySlime.transform.localScale = target;
    }
}
