using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SlimeShapeController))]
public class SlimeShapeControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        SlimeShapeController sb = (SlimeShapeController)target;
        if (GUILayout.Button("Update Shape"))
        {
            sb.UpdateVertices();
        }
    }
}