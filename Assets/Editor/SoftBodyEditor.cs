using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SoftBody))]
public class SoftBodyEditor : Editor
{
    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        SoftBody sb = (SoftBody)target;
        if (GUILayout.Button("Build Mesh")) {
            sb.buildMesh();
        }
    }
}
